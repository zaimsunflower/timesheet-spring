1. user (id, fname, lname, email)
2. task (id, name, description)
3. timesheet (id, project, user(fk), task(fk), status(fk), from, to)
4. status (id, name, code)
   1. OPEN, Open
   2. IN_PROGRESS, In progress
   3. CLOSED, Closed

constraint
(unique task, user)
timesheet -> task; task -> timesheet (unique foreign key task)
timesheet -> user; user -> timesheets (foreign key user)
timesheet -> status; status -> timesheets (foreign key status)