package com.example.timesheet.Task;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import java.util.UUID;

public class Task {
    private final UUID id;

    @NotBlank
    private final String name;
    private final String description;

    public Task(@JsonProperty("id") UUID id,
                @JsonProperty("name") String name,
                @JsonProperty("description") String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "Task {" +
                "id=" + id + '\'' +
                ", name=" + name + '\'' +
                ", description=" + description + '\'' +
                "}";
    }
}
