package com.example.timesheet.Task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.UUID;
import java.util.List;

@Repository
public class TaskDataAccessLayer {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public TaskDataAccessLayer(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    List<Task> selectAllTasks() {
        String sql = "" +
                "SELECT " +
                " id, " +
                " name, " +
                " description " +
                " FROM task";
        return jdbcTemplate.query(sql, mapTaskFromDb());
    }

    int insertTask(String taskId, Task task) {
        String sql = "" +
                "INSERT INTO task (" +
                " id, " +
                " name ) " +
                "VALUES (?, ?)";
        return jdbcTemplate.update(
                sql,
                taskId,
                task.getName()
        );
    }

    private RowMapper<Task> mapTaskFromDb() {
        return (resultSet, i) -> {
            String taskIdStr = resultSet.getString("id");
            UUID taskId = UUID.fromString(taskIdStr);
            String name = resultSet.getString("name");
            String description = resultSet.getString("description");
            return new Task(
                    taskId,
                    name,
                    description
            );
        };
    }
}
