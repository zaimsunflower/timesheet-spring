package com.example.timesheet.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("api/tasks")
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
public class TaskController {

    private final TaskService taskService;

    @Autowired
    public TaskController(TaskService taskService){
        this.taskService = taskService;
    }

    @GetMapping
    public List<Task> getAllTask(){
        return taskService.getAllTasks();
    }

    @PostMapping
    public Task addNewTask(@RequestBody @Valid Task task) {
        UUID taskId = UUID.randomUUID();
        String taskIdStr = taskId.toString();
        taskService.addNewTask(taskIdStr, task);
        Task createdTask = new Task(taskId, task.getName(), task.getDescription());
        return createdTask;
    }

}
