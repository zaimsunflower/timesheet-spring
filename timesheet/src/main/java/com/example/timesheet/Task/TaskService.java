package com.example.timesheet.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class TaskService {

    public final TaskDataAccessLayer taskDataAccessLayer;

    @Autowired
    public TaskService(TaskDataAccessLayer taskDataAccessLayer){
        this.taskDataAccessLayer = taskDataAccessLayer;
    }

    List<Task> getAllTasks(){
        return taskDataAccessLayer.selectAllTasks();
    }
    void addNewTask(String taskId, Task task) {
        taskDataAccessLayer.insertTask(taskId, task);
    }
}
