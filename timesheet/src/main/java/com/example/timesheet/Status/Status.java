package com.example.timesheet.Status;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.UUID;

public class Status {
    private final UUID id;
    private final String name;
    private final String code;

    public Status(@JsonProperty("id") UUID id,
                  @JsonProperty("name") String name,
                  @JsonProperty("code") String code) {
        this.id = id;
        this.name = name;
        this.code = code;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return "Status{" +
                "id=" + id + '\'' +
                ", name=" + name + '\'' +
                ", code=" + code + '\\' +
                "}";
    }
}
