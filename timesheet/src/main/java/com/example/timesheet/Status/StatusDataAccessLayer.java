package com.example.timesheet.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.UUID;

@Repository
public class StatusDataAccessLayer {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public StatusDataAccessLayer(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }

    List<Status> selectAllStatus(){
        String  sql = "" +
                "SELECT " +
                " id, " +
                " name," +
                " code " +
                "FROM status";
        return jdbcTemplate.query(sql, mapStatusFromDb());
    }

    private RowMapper<Status> mapStatusFromDb(){
        return (resultSet, i)->{
            String statusIdStr = resultSet.getString("id");
            UUID statusId = UUID.fromString(statusIdStr);
            String name = resultSet.getString("name");
            String code = resultSet.getString("code");
            return new Status(
                    statusId, name, code
            );
        };
    }
}
