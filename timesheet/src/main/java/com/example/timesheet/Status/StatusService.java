package com.example.timesheet.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class StatusService {
    private final StatusDataAccessLayer statusDataAccessLayer;

    @Autowired
    public StatusService(StatusDataAccessLayer statusDataAccessLayer) {
        this.statusDataAccessLayer = statusDataAccessLayer;
    }

    List<Status> getAllStatus() {
        return statusDataAccessLayer.selectAllStatus();
    }
}
