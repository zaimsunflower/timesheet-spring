package com.example.timesheet.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class UserService {

    private final UserDataAccessLayer userDataAccessLayer;

    @Autowired
    public UserService(UserDataAccessLayer userDataAccessLayer) {
        this.userDataAccessLayer = userDataAccessLayer;
    }

    List<User> getAllUsers() {
        return userDataAccessLayer.selectAllUsers();
    }

}
