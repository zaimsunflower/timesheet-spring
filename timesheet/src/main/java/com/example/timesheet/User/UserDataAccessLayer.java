package com.example.timesheet.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.UUID;

@Repository
public class UserDataAccessLayer {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public UserDataAccessLayer(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    List<User> selectAllUsers() {
        String sql = "" +
                "SELECT " +
                "id, " +
                "first_name, " +
                "last_name, " +
                "email " +
                "FROM user";

        return jdbcTemplate.query(sql, mapUserFomDb());
    }
    private RowMapper<User> mapUserFomDb() {
        return (resultSet, i) -> {
            String userIdStr = resultSet.getString("id");
            UUID userId = UUID.fromString(userIdStr);
            String firstName = resultSet.getString("first_name");
            String lastName = resultSet.getString("last_name");
            String email = resultSet.getString("email");
            return new User(
                    userId,
                    firstName,
                    lastName,
                    email
            );
        };
    }
}
