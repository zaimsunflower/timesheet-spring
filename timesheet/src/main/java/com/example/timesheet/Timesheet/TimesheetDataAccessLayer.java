package com.example.timesheet.Timesheet;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.UUID;
import java.util.List;
import java.util.Date;

@Repository
public class TimesheetDataAccessLayer {
    private final JdbcTemplate jdbcTemplate;

    public TimesheetDataAccessLayer(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    List<Timesheet> selectAllTimesheet() {
        String sql = "" +
                "SELECT " +
                "id, " +
                "project, " +
                "user_id, " +
                "task_id, " +
                "status_id, " +
                "from_date, " +
                "to_date " +
                "FROM timesheet";
        return jdbcTemplate.query(sql, mapTimesheetFromDb());
    }

    int deleteTimesheetById(UUID timesheetId) {
        String sql = "" +
                "DELETE FROM timesheet " +
                "WHERE id = ?";
        return jdbcTemplate.update(sql, timesheetId.toString());
    }

    int insertTimesheet(String timesheetId, Timesheet timesheet) {
        String sql = "" +
                "INSERT INTO timesheet (" +
                " id, " +
                " project, " +
                " task_id, " +
                " from_date, " +
                " to_date, " +
                " status_id, " +
                " user_id ) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?)";
        return jdbcTemplate.update(
                sql,
                timesheetId,
                timesheet.getProject(),
                timesheet.getTaskId().toString(),
                timesheet.getFromDate(),
                timesheet.getToDate(),
                timesheet.getStatusId().toString(),
                timesheet.getUserId().toString()
        );
    }
    int updateTimesheet(UUID timesheetId, Timesheet timesheet) {
        String sql = "" +
                "UPDATE timesheet " +
                "SET project = ? , " +
                "user_id = ? , " +
                "task_id = ? , " +
                "status_id = ? , " +
                "from_date = ? , " +
                "to_date = ? " +
                "WHERE id = ?";
        return jdbcTemplate.update(sql,
                timesheet.getProject(),
                timesheet.getUserId().toString(),
                timesheet.getTaskId().toString(),
                timesheet.getStatusId().toString(),
                timesheet.getFromDate(),
                timesheet.getToDate(),
                timesheetId.toString());
    }
    private RowMapper<Timesheet> mapTimesheetFromDb() {
        return (resultSet, i) -> {
            try {
                String timesheetIdStr = resultSet.getString("id");
                String userIdStr = resultSet.getString("user_id");
                String taskIdStr = resultSet.getString("task_id");
                String statusIdStr = resultSet.getString("status_id");
                UUID timesheetId = UUID.fromString(timesheetIdStr);
                UUID userId = UUID.fromString(userIdStr);
                UUID taskId = UUID.fromString(taskIdStr);
                UUID statusId = UUID.fromString(statusIdStr);
                String project = resultSet.getString("project");

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Date fromDate = dateFormat.parse(resultSet.getString("from_date"));
                Date toDate = dateFormat.parse(resultSet.getString("to_date"));
                return new Timesheet(
                        timesheetId, project, userId, taskId, statusId, fromDate, toDate
                );
            } catch (ParseException e) {
                System.out.println("Error parsing date");
                return null;
            }
        };
    }
}
