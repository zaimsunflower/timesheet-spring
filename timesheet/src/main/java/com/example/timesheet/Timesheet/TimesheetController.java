package com.example.timesheet.Timesheet;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("api/timesheet")
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
public class TimesheetController {
    private final TimesheetService timesheetService;

    public TimesheetController(TimesheetService timesheetService) {
        this.timesheetService = timesheetService;
    }

    @GetMapping
    public List<Timesheet> getAllTimesheet() {
        return timesheetService.getAllTimesheet();
    }

    @DeleteMapping("{timesheetId}")
    public UUID deleteStudent(@PathVariable("timesheetId") UUID timesheetId) {
        timesheetService.deleteStudent(timesheetId);
        return timesheetId;
    }

    @PostMapping
    public Timesheet addNewTimesheet(@RequestBody @Valid Timesheet timesheet) {
        UUID timesheetId = UUID.randomUUID();
        String timesheetIdStr = timesheetId.toString();
        timesheetService.addNewTimesheet(timesheetIdStr, timesheet);
        Timesheet createdTimesheet = new Timesheet(timesheetId,
                timesheet.getProject(),
                timesheet.getUserId(),
                timesheet.getTaskId(),
                timesheet.getStatusId(),
                timesheet.getFromDate(),
                timesheet.getToDate());
        return createdTimesheet;
    }

    @PutMapping(path = "{timesheetId}")
    public Timesheet updateStudent(@PathVariable("timesheetId") UUID timesheetId,
                                   @RequestBody Timesheet timesheet) {
        timesheetService.modifyTimesheet(timesheetId, timesheet);
        Timesheet updatedTimesheet = new Timesheet(timesheetId,
                timesheet.getProject(),
                timesheet.getUserId(),
                timesheet.getTaskId(),
                timesheet.getStatusId(),
                timesheet.getFromDate(),
                timesheet.getToDate());
        return updatedTimesheet;
    }

}
