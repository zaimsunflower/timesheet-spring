package com.example.timesheet.Timesheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.UUID;

@Service
public class TimesheetService {
    private final TimesheetDataAccessLayer timesheetDataAccessLayer;

    @Autowired
    public TimesheetService(TimesheetDataAccessLayer timesheetDataAccessLayer){
        this.timesheetDataAccessLayer = timesheetDataAccessLayer;
    }

    List<Timesheet> getAllTimesheet(){
        return timesheetDataAccessLayer.selectAllTimesheet();
    }

    void deleteStudent(UUID timesheetId) {
        timesheetDataAccessLayer.deleteTimesheetById(timesheetId);
    }

    void addNewTimesheet(String timesheetId, Timesheet timesheet) {
        timesheetDataAccessLayer.insertTimesheet(timesheetId, timesheet);
    }

    void modifyTimesheet(UUID timesheetId, Timesheet timesheet){
        timesheetDataAccessLayer.updateTimesheet(timesheetId, timesheet);
    }
}
