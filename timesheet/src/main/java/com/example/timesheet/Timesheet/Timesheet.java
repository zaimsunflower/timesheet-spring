package com.example.timesheet.Timesheet;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;
import java.util.UUID;

public class Timesheet {
    private final UUID id;
    private final String project;
    private final UUID userId;
    private final UUID taskId;
    private final UUID statusId;
    private final Date fromDate;
    private final Date toDate;

    public Timesheet(@JsonProperty("id") UUID id,
                     @JsonProperty("project") String project,
                     @JsonProperty("userId") UUID userId,
                     @JsonProperty("taskId") UUID taskId,
                     @JsonProperty("statusId") UUID statusId,
                     @JsonProperty("fromDate") Date fromDate,
                     @JsonProperty("toDate") Date toDate) {
        this.id = id;
        this.project = project;
        this.userId = userId;
        this.taskId = taskId;
        this.statusId = statusId;
        this.fromDate = fromDate;
        this.toDate = toDate;
    }

    public UUID getId() {
        return id;
    }

    public String getProject() {
        return project;
    }

    public UUID getUserId() {
        return userId;
    }

    public UUID getTaskId() {
        return taskId;
    }

    public UUID getStatusId() {
        return statusId;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public String toDateString() {
        return "Timesheet{" +
                "id=" + id + '\'' +
                "project=" + project + '\'' +
                "userId=" + userId + '\'' +
                "taskId=" + taskId + '\'' +
                "statusId=" + statusId + '\'' +
                "fromDate=" + fromDate + '\'' +
                "toDate=" + toDate + '\'' +
                "}";
    }
}
